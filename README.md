## LAN-SHARE in Golang
Easily share files to other devices in the same local area network.

## Usage
```
Usage of lan-share.exe:
  -port int
        specify the server listening port. (default 3000)
  -token string
        specify the private token. (default "token")

```

Example: `./lan-share.exe -port 80 -token private`

## 